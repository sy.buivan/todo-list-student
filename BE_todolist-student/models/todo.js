const mongoose = require("mongoose");
mongoose.connect("mongodb://localhost:27017/todoList");

const Schema = mongoose.Schema;

const todoSchema = new Schema(
  {
    _id: String,
    firstName: String,
    lastName: String,
    age: Number,
    classNum: String,
    avatar: String,
  },
  {
    collection: "todo",
  }
);

const todoModel = mongoose.model("todo", todoSchema);

module.exports = todoModel;
