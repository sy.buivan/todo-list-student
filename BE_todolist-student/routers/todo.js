const express = require("express");
const mongoose = require("mongoose");
const {
  getAllTodo,
  getTodo,
  addTodo,
  editTodo,
  deleteTodo,
  deleteAllTodo,
} = require("../controller/todoController");
const todo = express.Router();
var myId = mongoose.Types.ObjectId();

// get all data from database
todo.get("/", getAllTodo);

// get one todo from database
todo.get("/:id", getTodo);

// add data in database
todo.post("/add", addTodo);

// update todo
todo.put("/:id", editTodo);

// Remove data
todo.delete("/delete/:id", deleteTodo);

todo.delete("/delete-all", deleteAllTodo);

module.exports = todo;
