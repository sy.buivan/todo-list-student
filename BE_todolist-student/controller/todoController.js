const multer = require("multer");
const todoModel = require("../models/todo");

const storage = multer.diskStorage({
  destination: (_req, file, callBack) => {
    callBack(null, "./public/");
  },
  filename: (_req, file, callBack) => {
    const uniqueSuffix =
      Date.now() + "-" + Math.round(Math.random() * 1e9) + file.originalname;
    callBack(null, uniqueSuffix);
  },
});

const upload = multer({
  storage: storage,
  fileFilter: (_req, file, callBack) => {
    if (
      file.mimetype == "image/png" ||
      file.mimetype == "image/jpg" ||
      file.mimetype == "image/jpeg"
    )
      callBack(null, true);
    else {
      callBack(null, false);
      return callBack(new Error("Only .png, .jpg and .jpeg format allowed!"));
    }
  },
}).single("file");

const getAllTodo = (req, res) => {
  todoModel
    .find({})
    .then((todoList) => {
      if (todoList.length === 0) {
        res.json({
          msg: "Return list is empty",
        });
      } else {
        res.json({
          mgs: "Get data successfully",
          todoList: todoList,
        });
      }
    })
    .catch((err) => {
      res.status(500).json("error 500");
    });
};

//
const getTodo = (req, res) => {
  const id = req.params.id;

  todoModel
    .findOne({
      _id: id,
    })
    .then((todo) => {
      const { firstName, lastName, age, classNum, avatar } = todo;
      res.json({ firstName, lastName, age, classNum, avatar });
    })
    .catch((error) => {
      res.status(500).json("Error ", error);
    });
};

//  Add todo
const addTodo = (req, res) => {
  upload(req, res, (err) => {
    try {
      console.log("req bodyid, ", req.body);
      err ? console.log(err) : console.log("req bodyid, ", req.file.filename);
      const { firstName, lastName, age, classNum, _id } = req.body;
      todoModel
        .create({
          _id: _id,
          firstName: firstName,
          lastName: lastName,
          age: age,
          classNum: classNum,
          avatar: req.file.filename,
        })
        .then((data) => {
          res.json({
            msg: "Create todo student successfully",
          });
        })
        .catch((err) => {
          res.status(500).json("Error");
        });
    } catch (error) {}
  });
};

const editTodo = (req, res) => {
  const _id = req.params.id;
  upload(req, res, (err) => {
    const { firstName, lastName, age, classNum } = req.body;
    console.log(req.body);
    todoModel
      .findByIdAndUpdate(_id, {
        firstName: firstName,
        lastName: lastName,
        age: age,
        classNum: classNum,
        avatar: req?.file?.filename,
      })
      .then((data) => {
        res.json({
          msg: "Update successfully",
        });
      })
      .catch((error) => {
        res.status(500).json("Error ", error);
      });
  });
};

const deleteTodo = (req, res) => {
  const _id = req.params.id;
  console.log("delta");

  todoModel
    .deleteOne({
      _id: _id,
    })
    .then((data) => {
      res.json({
        msg: "Delete success",
      });
    })
    .catch((err) => {
      res.status(500).json("Error ", error);
    });
};

// delete all todo
const deleteAllTodo = (req, res) => {
  todoModel
    .deleteMany({})
    .then((data) => {
      res.json({
        msg: "Delete all success",
      });
    })
    .catch((err) => {
      res.status(500).json("Error ", error);
    });
};

module.exports = {
  getAllTodo,
  getTodo,
  addTodo,
  editTodo,
  deleteTodo,
  deleteAllTodo,
};
