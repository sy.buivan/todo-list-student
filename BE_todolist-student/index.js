const express = require("express");
const app = express();
var cors = require("cors");
const todo = require("./routers/todo");
var bodyParser = require("body-parser");
const port = 3001;

app.use(cors());
// parse application/x-www-form-urlencoded
app.use(bodyParser.urlencoded({ extended: false }));

// parse application/json
app.use(bodyParser.json());

app.use("/api/todo/", todo);
app.use(express.static('./public'))

app.listen(port, () => {
  console.log(`Example app listening o port ${port}`);
});
