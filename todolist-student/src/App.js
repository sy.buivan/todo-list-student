import { Route, Routes } from "react-router";
import { Link, NavLink } from "react-router-dom";
import "./App.css";
import TodoStudent from "./features/Todo";

function App() {
  return (
    <div className="App">
      <Routes>
        <Route path="/*" element={<TodoStudent />} />
      </Routes>
    </div>
  );
}

export default App;
