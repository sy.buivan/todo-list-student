import axiosClient from "./axiosClient";

const todoApi = {
  getAll() {
    return axiosClient.get("/todo");
  },

  getTodo(id) {
    return axiosClient.get(`/todo/${id}`);
  },

  addTodo(values) {
    console.log("Values:", values);
    return axiosClient.post("/todo/add", values, {
      headers: { "content-type": "multipart/form-data" },
    });
  },
  updateTodo(id, newTodo) {
    return axiosClient.put(`todo/${id}`, newTodo, {
      headers: { "content-type": "multipart/form-data" },
    });
  },

  deleteTodo(id) {
    console.log(id);
    return axiosClient.delete(`todo/delete/${id}`);
  },

  deleteAllTodo() {
    return axiosClient.delete("todo/delete-all");
  },
};

export default todoApi;
