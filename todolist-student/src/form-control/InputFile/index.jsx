import React, { useState } from "react";
import PropTypes from "prop-types";
import "./style.scss";
import { Controller } from "react-hook-form";
import { Input } from "@mui/material";

function InputFile({ name, errors, control }) {
  const [fileName, setFile] = useState(
    "https://kaokao.vn/img/img-default.jpeg"
  );
  const handleChange = (e) => {
    setFile(URL.createObjectURL(e.target.files[0]));
    console.log(fileName);
  };
  return (
    <div>
      <p>Avatar:</p>
      <input type="file" name={name} onChange={handleChange} />
      <img src={fileName} alt="" className="img-avatar" />
    </div>
  );
}

InputFile.propTypes = {};

export default InputFile;
