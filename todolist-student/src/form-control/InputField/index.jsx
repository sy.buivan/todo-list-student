import React from "react";
import PropTypes from "prop-types";
import { TextField } from "@mui/material";
import "./style.scss";
import { Controller } from "react-hook-form";

function InputField({ name, label, errors, control }) {
  const hasError = errors[name];
  return (
    <Controller
      control={control}
      name={name}
      render={({ field: { onChange, onBlur, ref, value } }) => (
        <TextField
          className="input-field"
          label={label}
          variant="outlined"
          margin="dense"
          value={value}
          onChange={onChange}
          onBlur={onBlur}
          error={!!hasError}
          helperText={errors[name]?.message}
        />
      )}
    />
  );
}

InputField.propTypes = {};

export default InputField;
