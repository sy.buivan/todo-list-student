import todoApi from "../../api/todoApi";

export const addStudent = async (student) => {
  await todoApi.addTodo(student);

  return {
    type: "ADD_NEW_STUDENT",
  };
};

export const updateStudent = (student) => {
  return {
    type: "UPDATE_STUDENT",
    payload: student,
  };
};

export const deleteStudent = (id) => {
  return {
    type: "DELETE_STUDENT",
    payload: id,
  };
};
