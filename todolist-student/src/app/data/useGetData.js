import { useState, useEffect } from "react";
import todoApi from "../../api/todoApi";

export default function useGetData(id) {
  const [data, setData] = useState([]);
  const [loading, setLoading] = useState(false);

  useEffect(() => {
    (async () => {
      const res = await todoApi.getTodo(id);
      console.log("res", res);
      setData(res.data);
      setLoading(true);
    })();
  }, []);
  return {
    data,
    loading,
  };
}
