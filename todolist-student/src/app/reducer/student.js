import { useState } from "react";
import todoApi from "../../api/todoApi";
import useGetData from "../data/useGetData";

const initialState = {
  listStudent: (async () => {
    const res = await todoApi.getAll();
    console.log("list data api", res.data.todoList);
    return res.data.todoList;
  })(),
};


console.log("initialState", initialState);

const studentReducer = (state = initialState, action) => {
  switch (action.type) {
    case "ADD_NEW_STUDENT": {
      const newList = [...state.listStudent];
      newList.push(action.payload);
      localStorage.setItem("listStudent", JSON.stringify(newList));
      return {
        ...state,
        listStudent: newList,
      };
    }

    case "UPDATE_STUDENT": {
      const newList = [...state.listStudent];
      const index = state.listStudent.findIndex(
        (item) => item.id === action.payload.id
      );
      console.log(action.payload);
      if (index >= 0) {
        newList[index] = action.payload;
      }
      localStorage.removeItem("listStudent");
      localStorage.setItem("listStudent", JSON.stringify(newList));
      return {
        ...state,
        listStudent: newList,
      };
    }

    case "DELETE_STUDENT": {
      console.log(action.payload);
      const newList = state.listStudent.filter(
        (item) => item.id !== action.payload
      );

      localStorage.removeItem("listStudent");
      localStorage.setItem("listStudent", JSON.stringify(newList));
      return {
        ...state,
        listStudent: newList,
      };
    }

    default:
      return state;
  }
};

export default studentReducer;
