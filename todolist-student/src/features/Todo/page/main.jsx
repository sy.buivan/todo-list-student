import {
  Dialog,
  DialogActions,
  DialogContent,
  IconButton,
  Typography,
} from "@mui/material";
import { Box } from "@mui/system";
import React, { useEffect, useState } from "react";
import { Container } from "react-bootstrap";
// import { useDispatch } from "react-redux";
import { Link, useNavigate } from "react-router-dom";
import { Button } from "reactstrap";
import todoApi from "../../../api/todoApi";
import TodoList from "../components/TodoList";

function MainPage(props) {
  // const listStudent = useSelector((state) => state.student.listStudent);
  const [isDeleteStudent, setIsDeleteStudent] = useState(false);

  const [data, setData] = useState([]);
  const [open, setOpen] = useState(false);

  useEffect(() => {
    (async () => {
      const res = await todoApi.getAll();
      if (res.status === 200) {
        setData(res.data.todoList);
      }
    })();
  }, [isDeleteStudent]);

  console.log("data", data);
  // const dispatch = useDispatch();
  const navigate = useNavigate();

  const handleTodoRemoveClick = async (id) => {
    // const action = deleteStudent(id);
    // dispatch(action);

    await todoApi.deleteTodo(id);
    setIsDeleteStudent((pre) => !pre);
  };

  const handleTodoUpdateClick = (todo) => {
    const editTodoUrl = `/${todo._id}`;
    navigate(editTodoUrl);
  };

  const handleRemoveAll = async () => {
    await todoApi.deleteAllTodo();
    setIsDeleteStudent((pre) => !pre);
  };
  return (
    <div>
      <Container className="text-center">
        <Box m={2}>
          <Link to="/add">
            <Button>Go to add new student</Button>
          </Link>
        </Box>
        {data === undefined ? (
          ""
        ) : (
          <Box mb={2}>
            <Typography variant="h6" mb={2}>
              There are {data?.length} students on the list
            </Typography>

            <Button onClick={() => setOpen((pre) => !pre)}>
              Remove all todoList
            </Button>
          </Box>
        )}

        <TodoList
          todoList={data}
          onTodoRemoveClick={handleTodoRemoveClick}
          onTodoUpdateClick={handleTodoUpdateClick}
        />
        <Dialog
          open={open}
          aria-labelledby="alert-dialog-title"
          aria-describedby="alert-dialog-description"
        >
          <Typography align="center" variant="h6" m={2}>
            Are you sure you want to delete all todo?
          </Typography>
          <DialogContent>
            <DialogActions>
              <IconButton onClick={() => handleRemoveAll()}>
                <Button variant="outlined" color="primary">
                  Agree
                </Button>
              </IconButton>
              <IconButton onClick={() => setOpen((pre) => !pre)}>
                <Button variant="outlined" color="error">
                  Close
                </Button>
              </IconButton>
            </DialogActions>
          </DialogContent>
        </Dialog>
      </Container>
    </div>
  );
}

MainPage.propTypes = {};

export default MainPage;
