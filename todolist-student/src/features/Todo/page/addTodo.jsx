import React, { useEffect, useState } from "react";
import PropTypes from "prop-types";
import TodoForm from "../components/TodoForm";
import { useSelector } from "react-redux";
import { useNavigate, useParams } from "react-router";
import { v4 as uuidv4 } from "uuid";
import todoApi from "../../../api/todoApi";
import useGetData from "../../../app/data/useGetData";

function AddTodo(props) {
  const { idStudent } = useParams();
  const navigate = useNavigate();
  const isAddTodo = !idStudent;

  const handleOnSubmit = async (values) => {
    if (!isAddTodo) {
      // const action = updateStudent(newValues);
      // dispatch(action);
      const newValues = { ...values };
      const { avatar, firstName, lastName, age, classNum, active } = newValues;
      console.log("active: ", active);
      const formData = new FormData();
      formData.append("firstName", firstName);
      formData.append("lastName", lastName);
      formData.append("age", age);
      formData.append("classNum", classNum);
      formData.append("active", active);
      if (!active) {
        formData.append("file", avatar, avatar?.name);
      }

      await todoApi.updateTodo(idStudent, formData);
    } else {
      // const action = addStudent(newValues);
      // dispatch(action);
      const newValues = { ...values };
      newValues._id = uuidv4();
      const { avatar, _id, firstName, lastName, age, classNum } = newValues;
      console.log("newValues: ", newValues);
      const formData = new FormData();

      formData.append("_id", _id);
      formData.append("file", avatar, avatar.name);
      formData.append("firstName", firstName);
      formData.append("lastName", lastName);
      formData.append("age", age);
      formData.append("classNum", classNum);
      await todoApi.addTodo(formData);
    }

    navigate("/");
  };

  const initialValuesAdd = {
    firstName: "",
    lastName: "",
    age: "",
    classNum: "",
    avatar: "",
  };
  return (
    <div>
      <div className="form-todo">
        <TodoForm
          isAddTodo={isAddTodo}
          formValues={initialValuesAdd}
          onSubmit={handleOnSubmit}
          idStudent={idStudent}
        />
      </div>
    </div>
  );
}

AddTodo.propTypes = {};

export default AddTodo;
