import React from "react";
import PropTypes from "prop-types";
import { Route, Routes } from "react-router";
import MainPage from "./page/main";
import AddTodo from "./page/addTodo";

function TodoStudent(props) {
  return (
    <div>
      <Routes>
        <Route path="" element={<MainPage />} />
        <Route path="add" element={<AddTodo />} />
        <Route path="/:idStudent" element={<AddTodo />} />
      </Routes>
    </div>
  );
}

TodoStudent.propTypes = {};

export default TodoStudent;
