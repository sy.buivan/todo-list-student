import { yupResolver } from "@hookform/resolvers/yup";
import { Button, IconButton } from "@mui/material";
import React, { useEffect, useState } from "react";
import { useForm } from "react-hook-form";
import { useDispatch } from "react-redux";
import { useNavigate } from "react-router-dom";
import * as yup from "yup";
import todoApi from "../../../../api/todoApi";
import InputField from "../../../../form-control/InputField";
import "./style.scss";

function TodoForm({ isAddTodo, formValues, onSubmit, idStudent }) {
  const navigate = useNavigate();
  const [initialValues, setInitialValues] = useState(formValues);
  const [active, setActive] = useState(false);

  useEffect(() => {
    (async () => {
      if (!isAddTodo) {
        const res = await todoApi.getTodo(idStudent);
        console.log("res", res.data);
        setInitialValues(res.data);
        setActive(true);
        reset(res.data);
      }
    })();
  }, []);
  const [preview, setPreview] = useState(
    "https://kaokao.vn/img/img-default.jpeg"
  );

  console.log("idStudent", idStudent);

  const [fileName, setFile] = useState(initialValues.avatar);

  //   handle submit form
  const handleOnSubmit = async (values) => {
    if (!onSubmit) return;

    const newValues = { ...values };
    newValues.avatar = fileName;
    newValues.active = active;
    onSubmit(newValues);
  };

  //   validateForm add student
  const validateForm = yup.object().shape({
    firstName: yup.string().required("This field is required"),
    lastName: yup.string().required("This field is required"),
    classNum: yup.string().required("This field is required"),
    age: yup.number().required("This field is required").nullable(),
    avatar: yup.mixed().required("File is required"),
  });

  const {
    handleSubmit,
    control,
    register,
    reset,
    formState: { errors },
  } = useForm({
    defaultValues: initialValues,
    resolver: yupResolver(validateForm),
  });

  console.log("initialValues", initialValues);

  return (
    <div className="form-group">
      <h2 className="form-group__title">
        {!isAddTodo
          ? `Edit student ${initialValues.firstName} ${initialValues.lastName}`
          : "Add new a student"}
      </h2>
      <form onSubmit={handleSubmit(handleOnSubmit)}>
        <div className="form-input">
          <InputField
            name="firstName"
            label="First name"
            control={control}
            errors={errors}
          />
        </div>
        <div className="form-input">
          <InputField
            name="lastName"
            label="Last name"
            control={control}
            errors={errors}
          />
        </div>
        <div className="form-input">
          <InputField
            name="age"
            label="Age"
            control={control}
            errors={errors}
          />
        </div>
        <div className="form-input">
          <InputField
            name="classNum"
            label="Class"
            control={control}
            errors={errors}
          />
        </div>
        <div>
          {/* <InputFile name="avatar" control={control} errors={errors} register={register} /> */}
          <div>
            <p>Avatar:</p>
            <input
              type="file"
              accept=".png, .jpg, .jpeg"
              {...register("avatar", {
                onChange: (event) => {
                  setFile(event.target.files[0]);
                  setPreview(URL.createObjectURL(event.target.files[0]));
                  setActive(false);
                },
              })}
            />

            <img
              src={
                !isAddTodo && active
                  ? `http://localhost:3001/${initialValues.avatar}`
                  : preview
              }
              alt=""
              className="img-avatar"
              width="100px"
              height="100px"
            />
            <p style={{ color: "#d32f2f", fontStyle: "italic" }}>
              {errors.avatar?.message}
            </p>
          </div>
        </div>

        <IconButton>
          <Button variant="outlined" color="primary" type="submit">
            {!isAddTodo ? "Edit" : "Submit"}
          </Button>
        </IconButton>
        <IconButton onClick={() => navigate("/")}>
          <Button variant="outlined" color="error">
            Back
          </Button>
        </IconButton>
      </form>
    </div>
  );
}

TodoForm.propTypes = {};

export default TodoForm;
