import React, { useState } from "react";
import { Button } from "reactstrap";
import "./style.scss";
import {
  Dialog,
  DialogActions,
  DialogContent,
  IconButton,
  Typography,
} from "@mui/material";

function TodoCard({ todo, onEditClick, onRemoveClick }) {
  const [openDel, setOpenDel] = useState(false);
  const handleEditClick = () => {
    if (!onEditClick) return;

    onEditClick(todo);
  };
  const handleRemoveClick = (id) => {
    if (!onEditClick) return;
    setOpenDel((pre) => !openDel);
    onRemoveClick(id);
  };
  
  return (
    <div className="todo">
      <div className="todo__overlay">
        <div className="todo__info">
          <p className="todo__info-text">
            <strong>First name: </strong>
            <span>{todo.firstName}</span>
          </p>
          <p className="todo__info-text">
            <strong>Last name: </strong>
            <span>{todo.lastName}</span>
          </p>
          <p className="todo__info-text">
            <strong>Age: </strong>
            <span>{todo.age}</span>
          </p>
          <p className="todo__info-text">
            <strong>Class: </strong>
            <span>{todo.classNum}</span>
          </p>

          <div className="todo__info-avt">
            <p className="todo__info-text">
              <strong>Avatar: </strong>
            </p>
            <img
              src={`http://localhost:3001/${todo.avatar}` || "https://kaokao.vn/img/img-default.jpeg"}
              alt=""
            />
          </div>
        </div>

        <div className="todo__actions">
          <div>
            <Button outline size="sm" color="success" onClick={handleEditClick}>
              Edit
            </Button>
          </div>

          <div>
            <Button
              outline
              size="sm"
              color="danger"
              onClick={() => setOpenDel((pre) => !pre)}
            >
              Remove
            </Button>
          </div>
        </div>
      </div>
      <Dialog
        open={openDel}
        aria-labelledby="alert-dialog-title"
        aria-describedby="alert-dialog-description"
      >
        <Typography align="center" variant="h6" m={2}>
          Are you sure you want to delete{" "}
          <span style={{ color: "red" }}>
            {todo.firstName} {todo.lastName}
          </span>
        </Typography>
        <DialogContent>
          <DialogActions>
            <IconButton onClick={() => handleRemoveClick(todo._id)}>
              <Button variant="outlined" color="primary">
                Agree
              </Button>
            </IconButton>
            <IconButton onClick={() => setOpenDel((pre) => !openDel)}>
              <Button variant="outlined" color="error">
                Close
              </Button>
            </IconButton>
          </DialogActions>
        </DialogContent>
      </Dialog>
    </div>
  );
}

export default TodoCard;
