import React from "react";
import PropTypes from "prop-types";
import { Col, Row } from "react-bootstrap";
import TodoCard from "../TodoCard/index";

function TodoList({ todoList, onTodoRemoveClick, onTodoUpdateClick }) {
  console.log("Todolist", todoList);
  return (
    <Row>
      {todoList?.length <= 0 || todoList === undefined ? (
        <h2>There are no students on the list yet</h2>
      ) : (
        todoList.map((todo) => (
          <Col key={todo._id} xs="12" md="4" lg="3">
            <TodoCard
              todo={todo}
              onEditClick={onTodoUpdateClick}
              onRemoveClick={onTodoRemoveClick}
            />
          </Col>
        ))
      )}
    </Row>
  );
}

TodoList.propTypes = {};

export default TodoList;
